function StaticPage() {

  return (
    <div>
      <h1 style={{ fontSize: '1em', fontWeight: 'bold', color: 'black', textAlign: 'left', margin: '20px' }}>Política de Aceptación de Servicio para la Plataforma BEES - HALO</h1>
      <p style={{maxWidth: '600px', color: 'black', textAlign: 'left', margin: '20px', marginTop: '25px', marginBottom: '30px' }}>
        1. Introducción
        <br />
        Esta Política de Aceptación de Servicio (la Política) describe los términos y condiciones bajo los cuales los usuarios pueden acceder y utilizar los servicios proporcionados por BEES - HALO (la Plataforma). Al acceder y utilizar la Plataforma, los usuarios aceptan cumplir con esta Política y cualquier otro acuerdo o término aplicable.
        <br />
        <br />
        2. Uso Aceptable
        <br />
        Los usuarios se comprometen a utilizar la Plataforma de manera legal, ética y conforme a las leyes y regulaciones aplicables. No se permite el uso de la Plataforma para actividades que sean ilegales, perjudiciales, difamatorias, fraudulentas o que violen los derechos de terceros.
        <br />
        <br />
        3. Registro y Cuentas de Usuario
        <br />
        Para acceder a ciertas características de la Plataforma, los usuarios pueden necesitar registrar una cuenta. Los usuarios son responsables de proporcionar información precisa y actualizada durante el proceso de registro, y de mantener la confidencialidad de sus credenciales de inicio de sesión.
        <br />
        <br />
        4. Propiedad Intelectual
        <br />
        Los usuarios entienden y aceptan que la Plataforma y todo su contenido, incluidos pero no limitados a textos, gráficos, logotipos, imágenes y software, están protegidos por derechos de propiedad intelectual y no pueden ser utilizados sin el consentimiento expreso de BEES IOT SAS.
        <br />
        <br />
        5. Privacidad y Seguridad
        <br />
        La Plataforma recopila y procesa información de los usuarios de acuerdo con nuestra Política de Privacidad. Nos comprometemos a mantener la seguridad de la información y a tomar medidas para protegerla contra accesos no autorizados o divulgación.
        <br />
        <br />
        6. Disponibilidad y Mantenimiento
        <br />
        Hacemos todo lo posible para garantizar la disponibilidad y el funcionamiento continuo de la Plataforma. Sin embargo, no garantizamos que la Plataforma esté libre de errores o interrupciones y nos reservamos el derecho de realizar mantenimiento programado.
        <br />
        <br />
        7. Limitación de Responsabilidad
        <br />
        En ningún caso seremos responsables por daños indirectos, consecuentes, ejemplares o incidentales que surjan del uso de la Plataforma.
        <br />
        <br />
        8. Modificaciones y Terminación
        <br />
        Nos reservamos el derecho de modificar o terminar la Plataforma, o cualquier parte de ella, en cualquier momento y sin previo aviso.
        <br />
        <br />
        9. Ley Aplicable y Jurisdicción
        <br />
        Esta Política se rige por las leyes de Argentina, y cualquier disputa que surja en relación con esta Política se someterá a la jurisdicción exclusiva de los tribunales de Argentina.
        <br />
        <br />
        10. Contacto
        <br />
        Para cualquier pregunta o inquietud sobre esta Política, por favor contáctanos en hola@bees.com.ar
      </p>
    </div>
  )
}

export default StaticPage
